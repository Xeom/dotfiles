#!/bin/bash

trap 'kill $(jobs -p)' EXIT


tmpdir=/tmp/i3-status

mkdir -p $tmpdir

function battery
{
    cat $tmpdir/battery
}

function battery_bg
{
    echo Hello > $tmpdir/battery
    while true; do
        percent=$(upower -i $1 | grep percentage | cut -d: -f2 | tr -d %)
        state=$(upower -i $1 | grep state | cut -d: -f2)
        if [[ "$state" == *discharging* ]]; then
            printf "Discharging %d%%" $percent > $tmpdir/battery
        elif [[ $state == *charging* ]]; then
            printf "Charging %d%%" $percent > $tmpdir/battery
        fi
        sleep 5
    done
}

batdev=$(upower -e | grep BAT)
battery_bg $batdev &

function updates_bg
{
    while true; do
        updates=$(/usr/lib/update-notifier/apt-check --human-readable | head -n 2 | cut -d" " -f1)
        printf "%d Updates, %d Security Updates" $updates > $tmpdir/updates
        sleep 600
    done
}

updates_bg &

function datetime
{
    date +'%Y %b %d %T'
}

function memusage
{
    free -h | grep Mem | awk '{printf $3"iB"}'
}

function updates
{
    cat $tmpdir/updates
}

function btstatus
{
    devices=$(bt-device -l | grep -oP "([A-F0-9]{2}:){5}[A-F0-9]{2}")
    for dev in $devices; do
        if bt-device -i $dev | grep -q "Connected: 1"; then
            printf "BT:"
            bt-device -i $dev | grep "Name" | cut -d":" -f2
        fi
    done
}

while true; do
    echo $(btstatus) \| $(updates) \| $(memusage) \| $(battery) \| $(datetime)
    sleep 1
done

