install_i3: i3/config
	mkdir -p ~/.i3/
	cp -f i3/config ~/.i3/config

install_emacs: emacs/config
	cp -f emacs/config ~/.emacs

install_x:
	cp -f x/xresources ~/.Xresources

install_bashrc:
	cp -f bashrc ~/.bashrc

install_gitconfig:
	cp -f gitconfig ~/.gitconfig

install_tmux:
	cp -f tmux/conf ~/.tmux.conf

install_vim:
	cp -f vim/rc ~/.vimrc
	mkdir -p ~/.config
	mkdir -p ~/.config/nvim
	cp -f vim/rc ~/.config/nvim/init.vim

install_dunst:
	mkdir -p ~/.config/dunst
	cp -f dunstrc ~/.config/dunst/dunstrc

install_zathura:
	cp -f zathurarc ~/.config/zathura/zathurarc

install_latexmk:
	cp -f latexmkrc ~/.latexmkrc

install: install_i3 install_emacs install_x install_bashrc install_gitconfig install_tmux install_vim install_dunst install_zathura install_latexmk

.DEFAULT_GOAL = install
